package com.rebhu.startupschool2018;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface API {
    @FormUrlEncoded
    @POST("/register.php")
    Single<Response<RegRespModel>> registerUser(@Field("email") String email, @Field("os-s") String os);
}
