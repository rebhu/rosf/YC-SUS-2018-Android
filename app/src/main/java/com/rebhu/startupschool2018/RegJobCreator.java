package com.rebhu.startupschool2018;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class RegJobCreator implements JobCreator {
    @Nullable
    @Override
    public Job create(@NonNull String tag) {
        switch (tag) {
            case Constants.REG_JOB_TAG:
                return new RegJob();
            default:
                return null;
        }
    }
}
