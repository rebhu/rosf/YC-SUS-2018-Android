package com.rebhu.startupschool2018;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class IntroFragment extends Fragment {


    public IntroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intro, container, false);

        FloatingActionButton floatingActionButtonNextIntroFragment =
                view.findViewById(R.id.floatingActionButtonNextIntroFragment);

        floatingActionButtonNextIntroFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frameLayoutLaunchActivityPlaceholder, new WelcomeFragment())
                        .commitNow();
            }
        });
        return view;
    }

}
