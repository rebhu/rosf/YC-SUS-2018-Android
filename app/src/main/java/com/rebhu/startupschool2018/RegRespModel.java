package com.rebhu.startupschool2018;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status"
})
public class RegRespModel {

    @JsonProperty("status")
    private String status;

    /**
     * No args constructor for use in serialization
     *
     */
    public RegRespModel() {
    }

    /**
     *
     * @param status
     */
    public RegRespModel(String status) {
        super();
        this.status = status;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

}
