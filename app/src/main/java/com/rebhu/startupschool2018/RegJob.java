package com.rebhu.startupschool2018;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.Objects;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class RegJob extends Job {

    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = preferences.edit();
        String email = preferences.getString(Constants.EMAIL, "invalid");

        if (Objects.equals(email, "invalid")) {
            editor.putBoolean(Constants.IS_REGISTERED, false).apply();
            return Result.FAILURE;
        } else {
            HTTPClient.getRetrofitClient().create(API.class).registerUser(email, "Android")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(getResponseSingleObserver(editor));
            return Result.SUCCESS;
        }
    }

    private SingleObserver<Response<RegRespModel>> getResponseSingleObserver(
            final SharedPreferences.Editor editor) {

        return new SingleObserver<Response<RegRespModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Response<RegRespModel> regResponse) {
                if (regResponse.isSuccessful()) {
                    RegRespModel regRespModel = regResponse.body();
                    switch (regRespModel.getStatus()) {
                        case "DBConnFail":
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                            new JobRequest.Builder(Constants.REG_JOB_TAG)
                                    .setExecutionWindow(30_000L, 40_000L)
                                    .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                                    .build()
                                    .schedule();
                            break;
                        case "conn-fail":
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                            new JobRequest.Builder(Constants.REG_JOB_TAG)
                                    .setExecutionWindow(30_000L, 40_000L)
                                    .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                                    .build()
                                    .schedule();
                            break;
                        case "duplicate":
                            Toast.makeText(getContext(), R.string.duplicate, Toast.LENGTH_LONG).show();
                            editor.putBoolean(Constants.IS_REGISTERED, false).apply();
                            editor.remove(Constants.EMAIL);
                            editor.apply();
                            break;
                        case "registered":
                            Toast.makeText(getContext(), R.string.registered, Toast.LENGTH_LONG).show();
                            editor.putBoolean(Constants.IS_REGISTERED, true);
                            editor.putBoolean(Constants.SHOULD_SKIP_WELCOME, true);
                            editor.remove(Constants.EMAIL);
                            editor.apply();
                            break;
                        default:
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                            new JobRequest.Builder(Constants.REG_JOB_TAG)
                                    .setExecutionWindow(30_000L, 40_000L)
                                    .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                                    .build()
                                    .schedule();
                            break;
                    }
                } else {
                    Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                    new JobRequest.Builder(Constants.REG_JOB_TAG)
                            .setExecutionWindow(30_000L, 40_000L)
                            .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                            .build()
                            .schedule();
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                new JobRequest.Builder(Constants.REG_JOB_TAG)
                        .setExecutionWindow(30_000L, 40_000L)
                        .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                        .build()
                        .schedule();
            }
        };
    }
}
