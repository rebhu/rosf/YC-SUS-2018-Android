package com.rebhu.startupschool2018;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class HTTPClient {

    private static final String TAG = "HTTPClient";
    private static final int TIMEOUT_MINUTES = 3;

    private static OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static Headers.Builder headersBuilder = new Headers.Builder();
    private static ConnectionSpec.Builder connectionSpecBuilder =
            new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS);
    private static MediaType MEDIA_TYPE_MSG_PACK = MediaType.parse("application/octet-stream");

    private static ConnectionSpec getConnectionSpec() {
        connectionSpecBuilder.tlsVersions(TlsVersion.TLS_1_2);
        connectionSpecBuilder.cipherSuites(
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_RC4_128_SHA,
                CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_DHE_DSS_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA);

        return connectionSpecBuilder.build();

    }

    public static Retrofit getRetrofitClient() {
        return new Retrofit.Builder()
                .client(getHTTPClient())
                .baseUrl("https://rebhu.com/")
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private static OkHttpClient getHTTPClient() {
        httpClientBuilder.readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES);
        httpClientBuilder.writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES);
        httpClientBuilder.connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES);
        httpClientBuilder.connectionSpecs(Collections.singletonList(getConnectionSpec()));
        httpClientBuilder.retryOnConnectionFailure(true);
        return httpClientBuilder.build();
    }

}
