package com.rebhu.startupschool2018;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnLongClickListener {
    private static final String URI="https://www.startupschool.org/";

    private FrameLayout frameLayoutVideoWebViewFragment;
    private View includeView;

    private WebView webView;
    private WebViewClient susWebViewClient;
    private WebChromeClient susWebChromeClient;
    private WebView.FindListener findListener;

    private FloatingActionButton floatingActionButtonSearchWebViewFragment;
    private EditText editTextSearchLayoutWebViewActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        floatingActionButtonSearchWebViewFragment =
                findViewById(R.id.floatingActionButtonSearchWebViewFragment);

        includeView = findViewById(R.id.includeSearchLayoutWebVIewFragment);
        includeView.setVisibility(View.GONE);

        ImageView imageViewNextSearchLayout = findViewById(R.id.imageViewNextSearchLayout);
        ImageView imageViewCancelSearchLayout = findViewById(R.id.imageViewCancelSearchLayout);

        editTextSearchLayoutWebViewActivity = findViewById(R.id.editTextSearchLayout);

        webView = findViewById(R.id.webViewFragment);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        setSusWebViewClient();
        setSUSWebChromeClient();
        webView.setWebViewClient(susWebViewClient);
        webView.setWebChromeClient(susWebChromeClient);

        webView.loadUrl(URI);
        webView.setHorizontalScrollBarEnabled(false);
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);

        floatingActionButtonSearchWebViewFragment.setOnClickListener(this);
        floatingActionButtonSearchWebViewFragment.setOnLongClickListener(this);

        imageViewNextSearchLayout.setOnClickListener(this);
        imageViewCancelSearchLayout.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (frameLayoutVideoWebViewFragment != null) {
            susWebChromeClient.onHideCustomView();
        } else if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingActionButtonSearchWebViewFragment:
                String searchUrl = "https://www.startupschool.org/forum/search";
                webView.loadUrl(searchUrl);
                break;


            case R.id.imageViewNextSearchLayout:
                String query = editTextSearchLayoutWebViewActivity.getText().toString().trim();

                if (!query.isEmpty()) {
                    webView.findAllAsync(query);
                    webView.findNext(true);
                }
                break;

            case R.id.imageViewCancelSearchLayout:
                webView.findAllAsync("");
                includeView.setVisibility(View.GONE);
                floatingActionButtonSearchWebViewFragment.show();
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.floatingActionButtonSearchWebViewFragment:
                floatingActionButtonSearchWebViewFragment.hide();
                includeView.setVisibility(View.VISIBLE);
                return true;

            default:
                return false;
        }
    }

    private void setSUSWebChromeClient() {
        susWebChromeClient = new WebChromeClient() {
            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
                frameLayoutVideoWebViewFragment = findViewById(R.id.frameLayoutVideoWebViewFragment);

                webView.setVisibility(View.GONE);
                floatingActionButtonSearchWebViewFragment.hide();
                WebViewActivity.this.getWindow().setFlags(
                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                frameLayoutVideoWebViewFragment.setVisibility(View.VISIBLE);
                frameLayoutVideoWebViewFragment.addView(view);
            }

            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
                webView.setVisibility(View.VISIBLE);
                floatingActionButtonSearchWebViewFragment.show();
                frameLayoutVideoWebViewFragment.setVisibility(View.GONE);
                frameLayoutVideoWebViewFragment = null;
            }
        };
    }

    private void setSusWebViewClient() {
        susWebViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return super.shouldOverrideUrlLoading(view, request);
            }
        };
    }

    private void setFindListener() {
        findListener = new WebView.FindListener() {
            @Override
            public void onFindResultReceived(int i, int i1, boolean b) {

            }
        };
    }
}
