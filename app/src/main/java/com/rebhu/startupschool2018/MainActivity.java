package com.rebhu.startupschool2018;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        boolean isFirstTime = preferences.getBoolean(Constants.IS_FIRST_TIME, true);
        boolean shouldSkipWelcome =
                preferences.getBoolean(Constants.SHOULD_SKIP_WELCOME, false);
        boolean isRegistered = preferences.getBoolean(Constants.IS_REGISTERED, false);

        if (isFirstTime) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameLayoutLaunchActivityPlaceholder, new IntroFragment())
                    .commitNow();
            editor.putBoolean(Constants.IS_FIRST_TIME, false);
            editor.apply();
        } else if (!shouldSkipWelcome && !isRegistered) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameLayoutLaunchActivityPlaceholder, new WelcomeFragment())
                    .commitNow();
        } else {
            startActivity(new Intent(this, WebViewActivity.class));
        }
    }
}
