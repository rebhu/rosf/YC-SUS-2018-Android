package com.rebhu.startupschool2018;

public class Constants {
    public static final String IS_FIRST_TIME = "is_first_time";
    public static final String SHOULD_SKIP_WELCOME = "should_skip_welcome";
    public static final String IS_REGISTERED = "is_registered";

    public static final String EMAIL = "email";
    public static final String REG_JOB_TAG = "reg_job";
}
